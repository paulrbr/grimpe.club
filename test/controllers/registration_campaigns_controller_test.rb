require "test_helper"

class RegistrationCampaignsControllerTest < ActionDispatch::IntegrationTest
  describe "Logged in as an admin member" do
    test "should get index" do
      sign_in_as(members(:janja)) do
        get registration_campaigns_url
        assert_response :success
      end
    end

    test "should get new form" do
      sign_in_as(members(:janja)) do
        get new_registration_campaign_url
        assert_response :success
      end
    end

    test "should be able to create" do
      sign_in_as(members(:janja)) do
        assert_difference("RegistrationCampaign.count") do
          post registration_campaigns_url,
            params: {
              registration_campaign: {
                sport_season_id: SportSeason.ongoing.id,
                start_at: Time.current,
                end_at: 1.week.from_now
              }
            }
        end
        reg = RegistrationCampaign.order(created_at: :desc).first
        assert_redirected_to registration_campaigns_path
        assert_equal members(:janja), reg.created_by
      end
    end
  end
end
