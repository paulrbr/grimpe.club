require "test_helper"

class SettingsTest < ActionDispatch::IntegrationTest
  test "Signed in as an admin can see & edit settings" do
    sign_in_as(members(:janja)) do
      get settings_path
    end
  end
end
