# This changes the default column type for primary keys,
# configuring your migration generator to set id: :uuid for new tables.
Rails.application.config.generators do |g|
  g.orm :active_record, primary_key_type: :uuid
end
