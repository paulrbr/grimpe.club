Rails.application.routes.draw do
  passwordless_for :members, at: "/", as: "passwordless"

  resources :members do
    collection do
      delete "batch"
      post "import"
      post "register"
    end
  end

  resources :groups do
    collection do
      delete "batch"
      post "import"
    end

    resources :memberships, only: [:create, :destroy]
  end

  resources :registrations

  resources :settings

  resources :registration_campaigns

  get "home", to: "root#home"

  root to: "root#index"
end
