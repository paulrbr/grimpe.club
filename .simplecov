require "simplecov-cobertura"

SimpleCov.coverage_dir("test/reports/coverage")
SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new(
  [
    SimpleCov::Formatter::HTMLFormatter,
    SimpleCov::Formatter::CoberturaFormatter
  ]
)
SimpleCov.start "rails" do
  add_filter "/test/"
  add_filter "/vendor/"
end
