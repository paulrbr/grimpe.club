class CreateRegistrationCampaigns < ActiveRecord::Migration[7.0]
  def change
    create_table :registration_campaigns, id: :uuid do |t|
      t.date :start_at, null: false
      t.date :end_at, null: false
      t.boolean :is_public, null: false, default: false
      t.integer :quota, null: true
      t.references :sport_season, references: :sport_seasons, type: :uuid, null: false, index: true
      t.references :created_by, references: :members, type: :uuid, null: false, index: true

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL.squish
          ALTER TABLE registration_campaigns
            ADD CONSTRAINT registration_campaign_positive_quota 
              CHECK (quota > 0);

          ALTER TABLE registration_campaigns
            ADD CONSTRAINT registration_campaign_non_empty_period
            CHECK (start_at < end_at);

          ALTER TABLE registration_campaigns
            ADD CONSTRAINT registration_campaign_overlapping_dates
            EXCLUDE USING GIST (
              (is_public::int) WITH =,
              TSRANGE(start_at, end_at) WITH &&
            );
        SQL
      end

      dir.down do
        execute <<-SQL.squish
          ALTER TABLE registration_campaigns
            DROP CONSTRAINT registration_campaign_positive_quota;

          ALTER TABLE registration_campaigns
            DROP CONSTRAINT registration_campaign_non_empty_period;

          ALTER TABLE registration_campaigns
            DROP CONSTRAINT registration_campaign_overlapping_dates;
        SQL
      end
    end
  end
end
