class CreateSportSeasons < ActiveRecord::Migration[7.0]
  def change
    create_table :sport_seasons, id: :uuid do |t|
      t.date :start_at, null: false
      t.date :end_at, null: false

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL.squish
          ALTER TABLE sport_seasons
            ADD CONSTRAINT non_empty_season_period
              CHECK (start_at < end_at);

          ALTER TABLE sport_seasons
            ADD CONSTRAINT season_overlapping_dates
              EXCLUDE USING GIST (
                TSRANGE(start_at, end_at) WITH &&
              );
        SQL
      end

      dir.down do
        execute <<-SQL.squish
          ALTER TABLE sport_seasons
            DROP CONSTRAINT non_empty_season_period;

          ALTER TABLE sport_seasons
            DROP CONSTRAINT season_overlapping_dates;
        SQL
      end
    end
  end
end
