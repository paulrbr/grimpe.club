class CreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :members, id: :uuid do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false, index: {unique: true}
      t.date :dob, null: false
      t.integer :sexe, null: false
      t.text :address, null: false
      t.string :zipcode, null: false
      t.string :city, null: false
      t.string :phone
      t.string :workflow_state, null: false
      t.integer :role, null: false

      t.references :parent, type: :uuid, index: true

      t.index "f_unaccent((first_name)::text) gin_trgm_ops", using: :gin
      t.index "f_unaccent((last_name)::text) gin_trgm_ops", using: :gin

      t.timestamps
    end
  end
end
