# frozen_string_literal: true

# This migration comes from passwordless (originally 20171104221735)

class CreatePasswordlessSessions < ActiveRecord::Migration[6.0]
  def connection
    ActiveRecord::Base.establish_connection(Rails.env.to_s.to_sym).connection
  end

  def change
    create_table :passwordless_sessions, id: :uuid do |t|
      t.belongs_to(
        :authenticatable,
        type: :uuid,
        polymorphic: true,
        index: {name: "authenticatable"}
      )
      t.datetime :timeout_at, null: false
      t.datetime :expires_at, null: false
      t.datetime :claimed_at
      t.text :user_agent, null: false
      t.string :remote_addr, null: false
      t.string :token, null: false

      t.timestamps
    end
  end
end
