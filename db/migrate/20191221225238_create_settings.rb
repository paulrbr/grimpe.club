class CreateSettings < ActiveRecord::Migration[6.0]
  def connection
    ActiveRecord::Base.establish_connection(Rails.env.to_s.to_sym).connection
  end

  def change
    create_table :settings, id: :uuid do |t|
      t.string :s_key
      t.binary :s_value

      t.timestamps
    end
  end
end
