class CreateMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :memberships, id: :uuid do |t|
      t.references :member, type: :uuid, null: false, index: true, foreign_key: true
      t.references :group, type: :uuid, null: false, index: true, foreign_key: true

      t.integer :year, null: false

      t.index [:member_id, :group_id, :year], unique: true

      t.timestamps
    end
  end
end
