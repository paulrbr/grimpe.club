class EnableExtensions < ActiveRecord::Migration[6.0]
  def connection
    ActiveRecord::Base.establish_connection("#{Rails.env}_admin".to_sym).connection
  end

  def change
    enable_extension "pgcrypto" unless extension_enabled?("pgcrypto")
    enable_extension "unaccent" unless extension_enabled?("unaccent")
    enable_extension "pg_trgm" unless extension_enabled?("pg_trgm")
    enable_extension "btree_gist" unless extension_enabled?("btree_gist")

    reversible do |dir|
      dir.up do
        # See this excellent reply about unaccent used in indexes
        # https://stackoverflow.com/questions/11005036/does-postgresql-support-accent-insensitive-collations/11007216#11007216
        execute <<-SQL.squish
          CREATE OR REPLACE FUNCTION public.immutable_unaccent(regdictionary, text)
          RETURNS text LANGUAGE c IMMUTABLE PARALLEL SAFE STRICT AS
          '$libdir/unaccent', 'unaccent_dict';

          CREATE OR REPLACE FUNCTION public.f_unaccent(text)
          RETURNS text LANGUAGE sql IMMUTABLE PARALLEL SAFE STRICT AS
          $func$
          SELECT public.immutable_unaccent(regdictionary 'public.unaccent', $1)
          $func$;
        SQL
      end
      dir.down do
        execute <<-SQL.squish
          DROP FUNCTION public.f_unaccent(text);
          DROP FUNCTION public.immutable_unaccent(regdictionary, text);
        SQL
      end
    end
  rescue => e
    Rails.logger.info("Something went wrong when enabling extensions: #{e.inspect}")
  end
end
