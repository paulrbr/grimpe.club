module RegistrationsHelper
  def render_registration_bootstrap_badge(registration)
    if registration.ongoing?
      text_key = "ongoing"
      color = "warning"
    elsif registration.ended?
      text_key = "ended"
      color = "secondary"
    elsif registration.future?
      text_key = "future"
      color = "success"
    end

    if color.present?
      text = t("activerecord.attributes.registration.#{text_key}")
      "<span class='badge badge-#{color}'>#{text}</span>"
    end
  end
end
