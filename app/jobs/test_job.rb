class TestJob < ApplicationJob
  queue_as :default

  def perform(*args)
    nb_candidates = Member.where(workflow_state: "candidate").count

    Rails.logger.info "There are currently #{nb_candidates} candidates"
  end
end
