# == Schema Information
#
# Table name: registrations
#
#  id         :uuid             not null, primary key
#  end_at     :date             not null
#  public     :boolean          default(FALSE), not null
#  quantity   :integer
#  start_at   :date             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  creator_id :uuid             not null
#
# Indexes
#
#  index_registrations_on_creator_id  (creator_id)
#  overlapping_dates                  (((public)::integer), tsrange((start_at)::timestamp without time zone, (end_at)::timestamp without time zone)) USING gist
#
class Registration < ApplicationRecord
  CREATION_FIELDS = [
    :start_at,
    :end_at,
    :quantity,
    :public
  ]

  belongs_to :creator, class_name: "Member"

  validate :end_date_cannot_be_in_the_past,
    :start_must_be_before_end,
    :positive_quantity

  scope :ongoing, -> { where(start_at: ..Time.current.to_date).where(end_at: Time.current.to_date..) }

  def ongoing?
    start_at.present? && start_at <= Time.current.to_date &&
      end_at.present? && Time.current.to_date <= end_at
  end

  def ended?
    end_at.present? && end_at < Time.current.to_date
  end

  def future?
    start_at.present? && Time.current.to_date < start_at
  end

  def period
    (end_at.to_date - start_at.to_date).to_i
  end

  def to_s
    "#<#{self.class}:#{id} public=#{public} quantity=#{quantity} start_at=#{start_at} end_at=#{end_at}>"
  end

  private

  def end_date_cannot_be_in_the_past
    if end_at.present? && end_at < Time.current.to_date
      errors.add(:end_at, I18n.t("activerecord.errors.registration.no_past"))
    end
  end

  def start_must_be_before_end
    if start_at.present? && end_at.present? && start_at > end_at
      errors.add(:start_at, I18n.t("activerecord.errors.registration.after_end"))
    end
  end

  def positive_quantity
    if quantity.present? && quantity < 0
      errors.add(:quantity, I18n.t("activerecord.errors.registration.quantity_must_be_positive"))
    end
  end
end
