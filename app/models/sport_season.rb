# == Schema Information
#
# Table name: sport_seasons
#
#  id         :uuid             not null, primary key
#  end_at     :date             not null
#  start_at   :date             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  season_overlapping_dates  (tsrange((start_at)::timestamp without time zone, (end_at)::timestamp without time zone)) USING gist
#
class SportSeason < ApplicationRecord
  def self.default_period(year)
    start_at = Date.new(year, 9, 1)
    end_at = Date.new(year + 1, 8, 31)

    new(start_at: start_at, end_at: end_at)
  end

  scope :future_seasons, -> {
    where(end_at: Time.current.to_date..)
  }

  def self.ongoing
    find_by(start_at: ..Time.current.to_date, end_at: Time.current.to_date..)
  end

  def to_label
    if start_at.year == end_at.year
      start_at.year.to_s
    else
      "#{start_at.year} - #{end_at.year}"
    end
  end
end
