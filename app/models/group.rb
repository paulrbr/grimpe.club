# == Schema Information
#
# Table name: groups
#
#  id          :uuid             not null, primary key
#  description :string
#  email       :string
#  name        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_groups_on_name  (name) UNIQUE
#
class Group < ApplicationRecord
  # Group model relationships
  has_many :memberships, dependent: :destroy
  has_many :members, through: :memberships
  has_many :current_members, -> {
    current.distinct
  }, through: :memberships, source: :member

  # Default group is a hidden group where registered member end up
  DEFAULT_GROUP_NAME = "default".freeze

  # Group model query interface (scopes)
  # By default, don't search for the "DEFAULT_GROUP"
  default_scope do
    where.not(name: DEFAULT_GROUP_NAME)
  end
  scope :default_group, -> { unscoped.where(name: DEFAULT_GROUP_NAME) }
  scope :current, -> { joins(:memberships).merge(Membership.current).distinct }

  # Group model validations
  validates :name, uniqueness: true
  validates :name, :description, presence: true
  validates :email, format: {with: URI::MailTo::EMAIL_REGEXP}, allow_blank: true, uniqueness: true

  # Full text search capabilities
  include PgSearch::Model
  pg_search_scope :search,
    against: [:name, :description],
    using: {
      tsearch: {prefix: true, any_word: true}
    }

  def self.default
    Group.default_group.first
  rescue ActiveRecord::RecordNotFound => e
    Rails.logger.warn <<~MSG
      #{e}
      You don't seem to have seeded initial database data. Try this:

      > bundle exec rake seed
    MSG
  end

  def is_default?
    name == DEFAULT_GROUP_NAME
  end
end
