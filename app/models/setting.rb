# == Schema Information
#
# Table name: settings
#
#  id         :uuid             not null, primary key
#  s_key      :string
#  s_value    :binary
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Setting < ApplicationRecord
  CONFIGURABLE_KEYS = {
    CONFIGURABLE_CLUB_NAME = "club.name" => "Grimpe.club",
    CONFIGURABLE_BETA_FEATURE = "beta.feature" => false,
    CONFIGURABLE_LISTING_PER_PAGE = "listing.per_page" => 20
  }.freeze

  before_save :clear_cache
  after_destroy :clear_cache

  validates :s_key, uniqueness: true
  validate :s_value, :non_empty_value

  def initialize(attributes = {})
    attr_key = attributes.delete(:key)
    attr_value = attributes.delete(:value)
    super(attributes)

    self.s_key = attr_key if attr_key
    self.value = attr_value if attr_value
  end

  def non_empty_value
    if !!value && value.blank?
      errors.add(:s_value, "can't be empty")
    end
  end

  def value=(val)
    self.s_value = Marshal.dump(val)
  end

  def value
    Marshal.load(s_value)
  end

  def parse_human_value!
    self.value = self.class.parse_human_value(value)
  end

  def clear_cache
    self.class.clear_cache(s_key)
  end

  def self.clear_cache(key)
    Rails.cache.delete_matched("#{name}/single/#{key}")
    Rails.cache.delete_matched("#{name}/all")
  end

  def self.cached_or_find_by_key(key)
    Rails.cache.fetch("#{name}/single/#{key}") do
      value = find_by(s_key: key)&.value
      value.nil? ? CONFIGURABLE_KEYS[key] : value
    end
  end

  def self.config
    custom = Rails.cache.fetch("#{name}/all") {
      custom = all # Select all settings from database
    }

    CONFIGURABLE_KEYS.map { |k, v|
      [
        k,
        custom.find do |s|
          s.s_key == k
        end&.value || v
      ]
    }.to_h
  end

  # Parse value as a string and try to guess the type of object entered
  def self.parse_human_value(value)
    if value.start_with?("[") && value.end_with?("]")
      value[1..-2].split(",").map(&:strip).map do |el|
        parse_human_value(el)
      end
    elsif ["true", "false"].include?(value)
      value == "true"
    elsif value.length == value.to_i.to_s.chars.count
      value.to_i
    else
      value
    end
  end
end
