# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(member)
    # Everybody can see index page (home)
    can :index, :root
    # When public registration period is ongoing everybody can create members
    if Registration.where(public: true).ongoing.exists?
      can :register, Member, {workflow_state: "candidate"}
    end

    # Unlogged members have no authorizations
    return if member.blank?

    # Authorized to read/update ourself and our childs
    can [:read, :update], Member, {id: member.id}
    can [:read, :update], Member, parent: {id: member.id}

    # Candidate or expired members can't do anything else
    return if member.candidate? || member.expired?

    # Authorized to read all groups
    can [:index, :read], Group, {memberships: {year: Time.current.year}}
    # Authorized to update its own group
    can [:update], Group, {memberships: {year: Time.current.year, member_id: member.id}}

    # Admin members only
    return unless member.admin?

    can :manage, :all
    #
    # The first argument to `can` is the action you are giving the member
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the member can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the member can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
