# == Schema Information
#
# Table name: registration_campaigns
#
#  id              :uuid             not null, primary key
#  end_at          :date             not null
#  is_public       :boolean          default(FALSE), not null
#  quota           :integer
#  start_at        :date             not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  created_by_id   :uuid             not null
#  sport_season_id :uuid             not null
#
# Indexes
#
#  index_registration_campaigns_on_created_by_id    (created_by_id)
#  index_registration_campaigns_on_sport_season_id  (sport_season_id)
#  registration_campaign_overlapping_dates          (((is_public)::integer), tsrange((start_at)::timestamp without time zone, (end_at)::timestamp without time zone)) USING gist
#
class RegistrationCampaign < ApplicationRecord
  belongs_to :sport_season
  belongs_to :created_by, class_name: "Member"

  validate :positive_quota

  scope :ongoing, -> { where(start_at: ..Time.current.to_date).where(end_at: Time.current.to_date..) }

  def positive_quota
    if quota.present? && quota < 0
      errors.add(
        :quota,
        I18n.t("activerecord.errors.registration_campaign.quota_must_be_positive")
      )
    end
  end
end
