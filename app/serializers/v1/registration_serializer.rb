class V1::RegistrationSerializer
  include FastJsonapi::ObjectSerializer

  # Private attributes
  attributes(*Registration::CREATION_FIELDS)
end
