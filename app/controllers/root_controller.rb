class RootController < ApplicationController
  def index
    redirect_to_home if current_user.present?

    @registration = ongoing_registration

    @member = current_user || Member.new
  end

  def home
    @registration = ongoing_registration
  end

  private

  def redirect_to_home
    redirect_to home_path
  end

  def ongoing_registration
    public_registration_only = current_user.blank? || current_user.candidate?

    Registration.where(public: public_registration_only).ongoing.first
  end
end
