class RegistrationCampaignsController < AuthenticatedController
  def index
    respond_to_html_json_csv(
      @registration_campaigns,
      V1::RegistrationCampaignSerializer
    )
  end

  def new
  end

  def create
    if @registration_campaign.save
      flash[:info] = I18n.t("registration_campaigns.create.success")

      redirect_to registration_campaigns_path
    else
      if @registration_campaign.errors.any?
        flash[:error] = render_activerecord_errors(@registration_campaign.errors)
      end

      render :new
    end
  rescue ActiveRecord::StatementInvalid
    flash[:error] = I18n.t("registration_campaigns.create.db_error")

    render :new
  end

  def show
  end

  private

  def create_params
    params
      .require(:registration_campaign)
      .permit(:start_at, :end_at, :quota, :is_public, :sport_season_id)
      .merge(created_by: current_user)
  end
end
