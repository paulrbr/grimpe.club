class SettingsController < AuthenticatedController
  def index
    @order = order_params[:order]

    if @order
      @settings = @settings.order(@order.to_h)
    end
  rescue ArgumentError
  end

  def create
    @setting.parse_human_value!

    if @setting.save
      flash[:info] = I18n.t("settings.create.success")
    else
      flash[:error] = render_activerecord_errors(@setting.errors)
    end

    redirect_to settings_path
  end

  def destroy
    @setting.destroy
    flash[:alert] = t(".success")

    redirect_to request.referer || settings_path
  end

  private

  def create_params
    params.require(:setting).permit(:key, :value)
  end

  def order_params
    params.permit(order: [:created_at, :s_key])
  end
end
