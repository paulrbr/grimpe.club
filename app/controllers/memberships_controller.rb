class MembershipsController < AuthenticatedController
  def create
    begin
      if @membership.save
        flash[:info] = I18n.t("groups.update.success")
      elsif @membership.errors.any?
        flash[:error] = render_activerecord_errors(@membership.errors)
      end
    rescue ActiveRecord::RecordNotUnique
      flash[:error] = I18n.t("groups.update.already_member")
    end

    redirect_to group_path(create_params[:group_id])
  end

  def destroy
    @membership.destroy

    if params[:group_id].present?
      redirect_to group_path(params[:group_id])
    end
  end

  private

  def create_params
    params.require(:membership)
      .permit(:member_id)
      .merge(
        group_id: params.require(:group_id),
        year: Time.current.year
      )
  end
end
