class ApplicationController < ActionController::Base
  # Generic helpers
  helper ApplicationHelper
  include ApplicationHelper

  # Authentication helpers
  include Passwordless::ControllerHelpers

  helper_method :current_user

  private

  def remember_previous_action
    session[:return_to] = request.referer
  end

  def current_user
    @current_user ||= authenticate_by_session(Member)
  end

  def require_user!
    remember_previous_action
    return if current_user
    save_passwordless_redirect_location!(Member)
    redirect_to send(Passwordless.mounted_as).sign_in_path, flash: {error: I18n.t("restricted_access")}
  end
end
